unit uFuncoesGenericas;

interface

uses Responsividade, fmx.forms,FMX.Controls;

  Type
  //Cria��o do Array de objetos como um objeto de responsividade
  TArrayResponsivity = Array of TResponsivity;

  //Fun��o que instancia todos os objetos do form de vez e deixa tudo dentro do array;
  TFuncoesGenericas = class
  class function tudoResponsivo(form:TForm;layout:TControl):TArrayResponsivity;
  end;

implementation

{ TFuncoesGenericas }

class function TFuncoesGenericas.tudoResponsivo(form:TForm;layout:TControl):TArrayResponsivity;
var i:integer;
    ArrayResponsivity:TArrayResponsivity;
begin
  //seta o tamanho do array
  SetLength(ArrayResponsivity,form.ComponentCount);
  //varre o array instanciando todos os objetos para realizar a fun��o de responsividade depois
  for I := 0 to form.ComponentCount-1 do
  begin
    if form.Components[i] is TControl then
    begin
      try
        ArrayResponsivity[i]:=TResponsivity.Create(TControl(form.Components[i]),layout);
      except end;
    end;
  end;
  result :=ArrayResponsivity;
end;

end.
